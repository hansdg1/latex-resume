module Jekyll

  module JFileSize

    # Returns the file size
    # set text=true to output as text
    # TODO fix file path when in a subdirectory
    def file_size(path, text=false)
      if text
        return filesize(File.size(path))
      else
        return File.size(path)
      end
    end

  end

end

Liquid::Template.register_filter(Jekyll::JFileSize)
